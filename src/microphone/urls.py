"""
    microphone URL Configuration
"""
from django.conf.urls import url, include
from django.contrib import admin

import upload.urls

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(upload.urls))
]
