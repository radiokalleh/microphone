from django.shortcuts import render
from django.http.response import HttpResponse, HttpResponseBadRequest

from .models import AudioUploadForm
from .utils import send_audio_to_headphone


def index(request):
    return render(request, 'index.html', {'form': AudioUploadForm()})


def upload(request):
    if request.method == 'POST':
        form = AudioUploadForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            send_audio_to_headphone(form.cleaned_data['title'],
                                    form.cleaned_data['artist'],
                                    request.FILES['audio_file'],
                                    request.FILES['cover_file'])
            return HttpResponse('Uploaded!')

        return HttpResponseBadRequest("Form is not valid")

    return HttpResponseBadRequest("Incorrect method")
