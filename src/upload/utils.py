import time

import grpc
from django.conf import settings
from headphone import headphone_pb2


def audio_chunk_generator(title, artist_name, audio_file, cover_file):
    details = headphone_pb2.AudioDetails(
        title=title,
        artistName=artist_name,
        coverURL=str(cover_file),
        id=str(time.time()))
    yield headphone_pb2.AddAudioRequest(details=details)

    for chunk in audio_file.chunks():
        bulk = headphone_pb2.AudioBulk(data=chunk)
        yield headphone_pb2.AddAudioRequest(bulk=bulk)


def send_audio_to_headphone(title, artist_name, audio_file, cover_file):
    cover_id = '{}_{}_{}'.format(time.time(), title, artist_name)
    settings.MINIO_CLIENT.put_object('audiocovers', cover_id, cover_file,
                                     cover_file.size, cover_file.content_type)

    settings.STUB.AddAudio(
        audio_chunk_generator(title, artist_name, audio_file, cover_id))
