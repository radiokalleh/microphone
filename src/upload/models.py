from django.db import models
from django.forms import ModelForm


class Audio(models.Model):
    title = models.CharField(max_length=200)
    artist = models.CharField(max_length=200)
    audio_file = models.FileField(upload_to='audio_files')
    cover_file = models.FileField(upload_to='cover_files')

    def __str__(self):
        return '[{} - {}]'.format(self.title, self.artist)


class AudioUploadForm(ModelForm):
    class Meta:
        model = Audio
        fields = ['title', 'artist', 'cover_file', 'audio_file']
